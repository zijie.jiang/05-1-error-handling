package com.twuc.webApp;

import com.twuc.webApp.model.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.I_AM_A_TEAPOT;

@RestController
@RequestMapping("/api/errors")
public class ExceptionHandlerController {

    @GetMapping("/default")
    public void exceptionMethod() {
        throw new RuntimeException("throw exception");
    }

    @GetMapping("/illegal-argument")
    public void throwIllegalArgumentExp() {
        throw new IllegalArgumentException("Something wrong with the argument");
    }

    @GetMapping("/null-pointer")
    public void throwNullPointException() {
        throw new NullPointerException("Something wrong with the argument");
    }

    @GetMapping("/arithmetic")
    public void throwArithmeticException() {
        throw new ArithmeticException("Something wrong with the argument");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleException(IllegalArgumentException exp) {
        return ResponseEntity.status(INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse("API-500", exp.getMessage()));
    }

    @ExceptionHandler({NullPointerException.class, ArithmeticException.class})
    public ResponseEntity<ErrorResponse> handleMultipleException(Exception e) {
        return ResponseEntity.status(I_AM_A_TEAPOT)
                .body(new ErrorResponse("API-418", e.getMessage()));
    }

}
