package com.twuc.webApp;


import com.twuc.webApp.model.ErrorResponse;
import com.twuc.webApp.model.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.I_AM_A_TEAPOT;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        return ResponseEntity.status(I_AM_A_TEAPOT)
                .contentType(APPLICATION_JSON)
                .body(new ErrorResponse("API-418", new Message(e.getMessage()).serialize()));
    }
}
