package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SisterController {

    @GetMapping("/sister-errors/illegal-argument")
    public void throwException() {
        throw new IllegalArgumentException("Something wrong with brother or sister.");
    }
}
