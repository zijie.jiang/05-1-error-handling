package com.twuc.webApp.model;

public class ErrorResponse {

    private String errorCode;
    private String errorMsg;

    public ErrorResponse() {

    }

    public ErrorResponse(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
