package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.ErrorResponse;
import com.twuc.webApp.model.Message;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class ExceptionHandlerControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_500_given_runtime_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/default", ErrorResponse.class);
        assertEquals(500, entity.getStatusCodeValue());
    }

    @Test
    void should_return_503_given_access_control_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/illegal-argument", ErrorResponse.class);
        assertEquals(500, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", Objects.requireNonNull(entity.getBody()).getErrorMsg());
    }

    @Test
    void should_return_418_given_null_point_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/null-pointer", ErrorResponse.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", Objects.requireNonNull(entity.getBody()).getErrorMsg());
    }

    @Test
    void should_return_418_given_arithmetic_exception() {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/errors/arithmetic", ErrorResponse.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", Objects.requireNonNull(entity.getBody()).getErrorMsg());
    }

    @Test
    void should_return_418_given_sister_illegal_argument_exception() throws IOException {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", ErrorResponse.class);
        assertEquals(418, entity.getStatusCodeValue());
        Message message = new ObjectMapper().readValue(Objects.requireNonNull(entity.getBody()).getErrorMsg(), Message.class);
        assertEquals("Something wrong with brother or sister.", message.getMessage());
    }

    @Test
    void should_return_418_given_brother_illegal_argument_exception() throws IOException {
        ResponseEntity<ErrorResponse> entity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", ErrorResponse.class);
        assertEquals(418, entity.getStatusCodeValue());
        Message message = new ObjectMapper().readValue(Objects.requireNonNull(entity.getBody()).getErrorMsg(), Message.class);
        assertEquals("Something wrong with brother or sister.", message.getMessage());
    }
}
